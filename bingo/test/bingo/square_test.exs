defmodule SquareTest do
  use ExUnit.Case, async: true

  alias Bingo.Square

  doctest Square

  describe "creating a square" do

    test "using new/2" do
      phrase = "A"
      points = 10

      square = Square.new(phrase, points)

      assert square.phrase == "A"
      assert square.points == 10
    end

    test "from a buzzword" do
      buzzword = %{phrase: "A", points: 10}

      square = Square.from_buzzword(buzzword)

      assert square.phrase == "A"
      assert square.points == 10
    end

    test "using new/2 when args are the wrong type" do
      phrase = 1
      points = "10"

      assert_raise FunctionClauseError, fn ->
        Square.new(phrase, points)
      end
    end

    test "from a buzzword when args are the wrong type" do
      buzzword = %{phrase: 1, points: "10"}

      assert_raise FunctionClauseError, fn ->
        Square.from_buzzword(buzzword)
      end
    end

  end

end
