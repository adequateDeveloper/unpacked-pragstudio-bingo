defmodule PlayerTest do
  use ExUnit.Case, async: true

  alias Bingo.Player

  doctest Player

  describe "creating a player" do

    test "using new/2" do
      name = "A"
      color = "g"

      player = Player.new(name, color)

      assert player.name == "A"
      assert player.color == "g"
    end

    test "using new/2 with args of the wrong type" do
      assert_raise FunctionClauseError, fn ->
        Player.new("A", 2)
      end

      assert_raise FunctionClauseError, fn ->
        Player.new(1, "g")
      end
    end

  end

end
