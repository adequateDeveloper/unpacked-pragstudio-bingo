defmodule ImplTest do
  use ExUnit.Case, async: true

  alias Bingo.BuzzwordCache.Impl

  doctest Impl

  test "getting the cached buzzwords" do
    buzzwords = Impl.get_buzzwords()

    assert %{phrase: _phrase, points: _points} = List.first(buzzwords)
  end

end
