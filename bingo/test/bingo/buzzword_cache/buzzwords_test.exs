defmodule BuzzwordsTest do
  use ExUnit.Case, async: true

  alias Bingo.BuzzwordCache.Buzzwords

  doctest Buzzwords

  test "parsing buzzwords" do
    buzzwords = "Above My Pay Grade,400\nAction Item,350\nActionable,100"

    actual = Buzzwords.parse_buzzwords(buzzwords)

    expected = [
                 %{phrase: "Above My Pay Grade", points: 400},
                 %{phrase: "Action Item", points: 350},
                 %{phrase: "Actionable", points: 100}
               ]

    assert actual == expected
  end

end
