defmodule Bingo.Mixfile do
  use Mix.Project

  def project do
    [
      app: :bingo,
      version: "0.1.0",
      elixir: "~> 1.6",
      test_coverage: [tool: ExCoveralls],
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {Bingo.Application, []}
    ]
  end

  defp deps do
    [
      {:ex_doc, "~> 0.18.3"},
      {:excoveralls, "~> 0.8.1", only: :test}
    ]
  end
end
