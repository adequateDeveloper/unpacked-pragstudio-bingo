defmodule Bingo.Player do
  @moduledoc """
  A game player containing a name and color choice.
  """
  
  @enforce_keys [:name, :color]
  defstruct [:name, :color]

  alias __MODULE__

  @doc """
  Creates a player with the given `name` and `color`.
  """
  def new(name, color) when is_binary(name) and is_binary(color) do
    %Player{name: name, color: color}
  end

end
