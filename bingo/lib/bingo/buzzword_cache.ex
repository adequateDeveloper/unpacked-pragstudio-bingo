defmodule Bingo.BuzzwordCache do
  @moduledoc """
  A process that loads a collection of buzzwords from an external source
  and caches them for expedient access. The cache is automatically 
  refreshed every hour.
  """

  @server_module :"Elixir.Bingo.BuzzwordCache.Server"

  def get_buzzwords() do
    GenServer.call(@server_module, :get_buzzwords)
  end

end
