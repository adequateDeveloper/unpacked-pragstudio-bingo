defmodule Bingo.BuzzwordCache.Server do
  @moduledoc """
  A process that loads a collection of buzzwords from an external source
  and caches them for expedient access. The cache is automatically
  refreshed every hour.
  """

  use GenServer

  alias Bingo.BuzzwordCache.Impl

  @refresh_interval :timer.minutes(60)

  def start_link(_arg) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    state = Impl.get_buzzwords()
    schedule_refresh()
    {:ok, state}
  end

  def handle_call(:get_buzzwords, _from, state) do
    {:reply, state, state}
  end

  def handle_info(:refresh, _state) do
    state = Impl.get_buzzwords()
    schedule_refresh()
    {:noreply, state}
  end

  defp schedule_refresh do
    Process.send_after(self(), :refresh, @refresh_interval)
  end

end
