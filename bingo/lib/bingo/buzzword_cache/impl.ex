defmodule Bingo.BuzzwordCache.Impl do
  @moduledoc """
  A process that loads a collection of buzzwords from an external source
  and caches them for expedient access. The cache is automatically 
  refreshed every hour.
  """

  alias Bingo.BuzzwordCache.Buzzwords

  def get_buzzwords() do
    Buzzwords.read_buzzwords()
  end

end
