defmodule Bingo.Game do
  @moduledoc """
  The core game logic. A new game is created, then game squares are marked for players and scores are tallied.
  """

  @compile if Mix.env() == :test, do: :export_all

  # scores contains %{<"name"> => points} i.e. %{"Mike" => 50, "Nicole" => 30}
  # winner contains a Player struct
  @enforce_keys [:squares]
  defstruct [squares: nil, scores: %{}, winner: nil]

  alias Bingo.{BingoChecker, BuzzwordCache, Game, Square}

  @doc """
  Creates a game with a `size` x `size` collection of squares 
  taken randomly from the given list of `buzzwords` where
  each buzzword is of the form `%{phrase: "Upsell", points: 10}`.
  """
  def new(size) when is_integer(size) do
    buzzwords = BuzzwordCache.get_buzzwords()
    new(buzzwords, size)
  end

  @doc """
  Creates a game with a `size` x `size` collection of squares 
  taken randomly from the given list of `buzzwords` where
  each buzzword is of the form `%{phrase: "Upsell", points: 10}`.
  """
  def new(buzzwords, size) when is_list(buzzwords) and is_integer(size) do
  IO.inspect(label: "inside new/2")
    squares = 
      buzzwords
      |> Enum.shuffle()
      |> Enum.take(size * size)
  |> IO.inspect(label: "after Enum.take")
      |> Enum.map(&Square.from_buzzword(&1))
  |> IO.inspect(label: "after Enum.map")
      |> Enum.chunk_every(size)
  |> IO.inspect(label: "after Enum.chunck_every")

    %Game{squares: squares}
  end

  @doc """
  Marks the square that has the given `phrase` for the given `player`,
  updates the scores, and checks for a bingo!
  """
  def mark(game, phrase, player) when is_map(game) and is_binary(phrase) and is_map(player) do
  IO.inspect(label: "inside mark/3")
    game
    |> update_squares_with_mark(phrase, player)
    |> update_scores()
    |> assign_winner_if_bingo(player)
  |> IO.inspect(label: "after assign_winner_if_bingo")
  end

  defp update_squares_with_mark(game, phrase, player) do
  IO.inspect(label: "inside update_squares_with_mark/3")
    new_squares =
      game.squares
      |> List.flatten()
  |> IO.inspect(label: "after List.flatten")
      |> Enum.map(&mark_square_having_phrase(&1, phrase, player))
  |> IO.inspect(label: "after mark_square_having_phrase")
      |> Enum.chunk_every(Enum.count(game.squares))
  |> IO.inspect(label: "after Enum.chunk_every")

    %{game | squares: new_squares}
  end

  defp mark_square_having_phrase(square, phrase, player) do
  IO.inspect(label: "inside mark_square_having_phrase/3")
  IO.inspect(label: "before case square.phrase == phrase do")
  IO.inspect(square, label: "square")
  IO.inspect(phrase, label: "phrase")
  IO.inspect(player, label: "player")
    case square.phrase == phrase do
      true -> %Square{square | marked_by: player}
      false -> square
    end
  end

  defp update_scores(game) do
  IO.inspect(label: "inside update_scores/1")
    scores =
      game.squares
      |> List.flatten()
  |> IO.inspect(label: "after List.flatten")
      |> Enum.reject(&is_nil(&1.marked_by))
  |> IO.inspect(label: "after Enum.reject")
      |> Enum.map(fn s -> {s.marked_by.name, s.points} end)
  |> IO.inspect(label: "after Enum.map")
      |> Enum.reduce(%{}, fn {name, points}, scores ->
           Map.update(scores, name, points, &(&1 + points))
  |> IO.inspect(label: "after Map.update")
         end)
  |> IO.inspect(label: "after Enum.reduce")

    %{game | scores: scores}
  end

  defp assign_winner_if_bingo(game, player) do
    case BingoChecker.bingo?(game.squares) do
      true  -> %{game | winner: player}
      false -> game
    end
  end
end
