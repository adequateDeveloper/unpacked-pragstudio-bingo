defmodule Bingo.BingoChecker do

  @compile if Mix.env() == :test, do: :export_all

  @doc """
  Checks for a bingo!

  Given a 2D list of `squares`, returns `true` if all the squares of
  any row, column, or diagonal have been marked by the same player.
  Otherwise `false` is returned.
  """
  def bingo?(squares) when is_list(squares) do
    possible_winning_square_sequences(squares)
    |> sequences_with_at_least_one_square_marked()
    |> Enum.map(&all_squares_marked_by_same_player?(&1))
    |> Enum.any?()
  end

  @doc """
  Given a 2D list of `squares`, returns a 2D list of all possible winning square sequences: rows, columns, left diagonal, and right diagonal.
  """
  defp possible_winning_square_sequences(squares) do
    squares ++  # rows
    transpose(squares) ++  # columns
    [left_diagonal_squares(squares), right_diagonal_squares(squares)]
  end

  @doc """
  Given a list of possible winning square sequences, returns a list of
  those sequences that have at least one square marked.
  """
  defp sequences_with_at_least_one_square_marked(squares) do
    Enum.reject(squares, fn sequence ->
      Enum.reject(sequence, &is_nil(&1.marked_by)) |> Enum.empty?()
    end)
  end

  @doc """
  Given a list of possible winning square sequences, returns `true` if 
  the sequence has all squares marked by the same player. 
  Otherwise, returns `false`.
  """
  defp all_squares_marked_by_same_player?(squares) do
  IO.inspect(squares, label: "inside all_squares_marked_by_same_player?/1")
    first_square = Enum.at(squares, 0)
  IO.inspect(first_square, label: "after Enum.at")
    Enum.all?(squares, fn s ->
      s.marked_by == first_square.marked_by
    end)
  |> IO.inspect(label: "after Enum.all?")
  end

  @doc """
  Given a 2D list of elements, returns a new 2D list where the 
  row and column indices have been switched. In other words,
  it flips the given matrix over its left diagonal.
  i.e.
             1  2  3         1  4  7
             4  5  6   ==>   2  5  8
             7  8  9         3  6  9

  ## Example
      iex> m = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
      iex> Bingo.BingoChecker.transpose(m)
      [[1, 4, 7], [2, 5, 8], [3, 6, 9]]
  """
  defp transpose(squares) do
    squares
  |> IO.inspect(label: "inside transpose/1")
    |> List.zip()
  |> IO.inspect(label: "after List.zip")
    |> Enum.map(&Tuple.to_list/1)
  |> IO.inspect(label: "after Enum.map")
  end

  @doc """
  Rotates the given 2D list of elements 90 degrees.
  i.e.
             1  2  3         3  6  9
             4  5  6   ==>   2  5  8
             7  8  9         1  4  7

  ## Example
      iex> m = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
      iex> Bingo.BingoChecker.rotate_90_degrees(m)
      [[3, 6, 9], [2, 5, 8], [1, 4, 7]]
  """
  defp rotate_90_degrees(squares) do
    squares
  |> IO.inspect(label: "inside rotate_90_degrees/1")
    |> transpose()
  |> IO.inspect(label: "after transpose")
    |> Enum.reverse()
  |> IO.inspect(label: "after Enum.reverse")
  end

  @doc """
  Returns the elements on the left diagonal of the given 2D list.
  i.e.
             1  2  3
             4  5  6   ==>   [1, 5, 9]
             7  8  9

  ## Example
      iex> m = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
      iex> Bingo.BingoChecker.left_diagonal_squares(m)
      [1, 5, 9]
  """
  defp left_diagonal_squares(squares) do
    squares
  |> IO.inspect(label: "inside left_diagonal_squares/1")
    |> List.flatten()
  |> IO.inspect(label: "after List.flatten")
    |> Enum.take_every(Enum.count(squares) + 1)
  |> IO.inspect(label: "after Enum.take_every")
  end

  @doc """
  Returns the elements on the right diagonal of the given 2D list.
  i.e.
             1  2  3
             4  5  6   ==>   [3, 5, 7]
             7  8  9

  ## Example
      iex> m = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
      iex> Bingo.BingoChecker.right_diagonal_squares(m)
      [3, 5, 7]
  """
  defp right_diagonal_squares(squares) do
    squares
  |> IO.inspect(label: "inside right_diagonal_squares/1")
    |> rotate_90_degrees()
    |> left_diagonal_squares()
  end
end
