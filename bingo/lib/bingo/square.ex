defmodule Bingo.Square do
  @moduledoc """
  A gameboard square containing a buzzword phrase and point value.
  """

  @enforce_keys [:phrase, :points]
  defstruct [:phrase, :points, :marked_by]  # :marked_by contains a Player struct

  alias __MODULE__

  @doc """
  Creates a square from the given `phrase` and `points`.
  """
  def new(phrase, points) when is_binary(phrase) and is_number(points) do
    %Square{phrase: phrase, points: points}
  end

  @doc """
  Creates a square from the given map having `:phrase` and `:points` keys.
  """
  def from_buzzword(%{phrase: phrase, points: points}) when is_binary(phrase) and is_number(points) do
    new(phrase, points)
  end
end
