
excoveralls: $ MIX_ENV=test mix coveralls

$ iex -S mix
> :observer.start


BuzzwordCache and Buzzwords
---------------------------
Refactored the BuzzwordCache into its 3 constituent parts: client api, business implementation, and GenServer.

  From:

    lib
      |__bingo
           |_buzzword_cache.ex     -- client api, business implementation, & GenServer


  To:

    lib
      |__bingo
           |_buzzword_cache.ex     -- client api
           |_buzzword_cache
                  |_impl.ex        -- business implementation
                  |_server.ex      -- GenServer
                  |_buzzwords.ex   -- buzzwords loader

Refactored code so only impl.ex makes calls to buzzwords.ex
